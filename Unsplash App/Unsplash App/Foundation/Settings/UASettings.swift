//
//  UASettings.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 3/3/21.
//

import Foundation

import UIKit

struct UASettings {
    
    static var current: UASettings = .init()
    
    var user: UserState = .none
}
