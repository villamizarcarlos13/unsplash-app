//
//  UIViewController+CE.swift
//  Cupo Express
//
//  Created by Freddy Silva on 5/6/20.
//  Copyright © 2020 Freddy Silva. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func validate(_ textfields: UITextField...) -> Bool {
        for textField in textfields {
            guard let text = textField.text else { return false }
            
            if text.isEmpty {
                return false
            }
        }
        
        return true
    }
    
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func add(child: UIViewController, container: UIView) {
        addChild(child)
        container.addSubview(child.view)
        child.didMove(toParent: self)
    }

    func remove() {
        guard parent != nil else {
            return
        }

        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}
