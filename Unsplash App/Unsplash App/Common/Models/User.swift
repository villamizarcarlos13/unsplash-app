//
//  User.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 3/3/21.
//

import Foundation

struct UserModels : Codable {
    let id                  : String?
    let username            : String?
    let name                : String?
    let first_name          : String?
    let last_name           : String?
    let profile_image       : ImageProfile?
    let location            : String?
    let total_collections   : Int?
    let total_likes         : Int?
    let total_photos        : Int?
    let photos              : [PhotosModels]?
}

enum UserState: Equatable {
    
    case logedIn(AllData.User.Response)
    case none
    
    static func == (lhs: UserState, rhs: UserState) -> Bool {
        switch lhs {
        case .none:
            guard case .none = rhs else {
                return false
            }
            return true
        case .logedIn:
            guard case .logedIn = rhs else {
                return false
            }
            return true
        }
    }
}
