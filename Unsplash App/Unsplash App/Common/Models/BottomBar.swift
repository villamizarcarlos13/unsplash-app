//
//  BottomBar.swift
//  Food Wallet
//
//  Created by Carlos Villamizar on 16/1/21.
//

import UIKit

enum ItemBottomBar: CaseIterable {
    case home
    case favorites
    case profile
    
    var image: UIImage {
        switch self {
        case .home:
            let image = UIImage(named: "home")!
            return image
        case .favorites:
            let image = UIImage(named: "favorite")!
            return image
        case .profile:
            let image = UIImage(named: "settings")!
            return image
        }
        
    }
    
    var name: String {
        switch self {
        case .home:
            return "Home"
        case .favorites:
            return "Favorites"
        case .profile:
            return "Profile"
        }
    }
    
    var isEnable: Bool {
        switch self {
        case .home:
            return true
        case .favorites:
            return true
        case .profile:
            return true
        }
    }
}
