//
//  Characters.swift
//  Marvel Characters
//
//  Created by Carlos Villamizar on 11/11/20.
//

import Foundation

struct AllPhotosData : Codable {
    let id              : String
    let urls            : ImagesData?
    let likes           : Int?
    let user            : UserModels?
    let description     : String?
    let collection      : [Collection]?
}

struct ImagesData : Codable {
    let full    : String?
    let raw     : String?
}

struct ImageProfile : Codable {
    let medium : String?
}

struct Collection : Codable {
    let id : String?
}

struct PhotosModels : Codable {
    let id      : String?
    let urls    : ImagesData?
}
