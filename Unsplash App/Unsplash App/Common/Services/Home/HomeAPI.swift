//
//  CharactersAPI.swift
//  Marvel Characters
//
//  Created by Carlos Villamizar on 11/11/20.
//

import Foundation

class HomeAPI: HomeServiceProtocol {
    func getPhotos(request: AllData.Photos.Request, completion: @escaping (Result<[AllData.Photos.Response], HomeError>) -> Void) {
        NetworkService.share.request(endpoint: HomeEndpoint.photos) { result in
            switch result {
            case .success:
                do {
                    let data = try result.get()
                    let response = try JSONDecoder().decode([AllData.Photos.Response].self, from: data!)
                    completion(.success(response))
                } catch let error {
                    completion(.failure(.parse(error)))
                }
            case .failure(let error):
                completion(.failure(.network(error)))
            }
        }
    }
    
    func getUser(request: AllData.User.Request, completion: @escaping (Result<AllData.User.Response, HomeError>) -> Void) {
        NetworkService.share.request(endpoint: HomeEndpoint.user) { result in
            switch result {
            case .success:
                do {
                    let data = try result.get()
                    let response = try JSONDecoder().decode(AllData.User.Response.self, from: data!)
                    completion(.success(response))
                } catch let error {
                    completion(.failure(.parse(error)))
                }
            case .failure(let error):
                completion(.failure(.network(error)))
            }
        }
    }
}
