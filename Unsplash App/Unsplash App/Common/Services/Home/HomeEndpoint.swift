//
//  CharactersEndpoint.swift
//  Marvel Characters
//
//  Created by Carlos Villamizar on 11/11/20.

import Foundation
import Alamofire

enum HomeEndpoint {
    case photos
    case user
}

extension HomeEndpoint: IEndpoint {
    var method: HTTPMethod {
        switch self {
        case .photos, .user:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .photos:
            return "photos"
        case .user:
            return "users/olenkasergienko"
        }
    }
    
    var parameter: Parameters? {
        switch self {
        case .photos, .user:
            return nil
        }
    }
    
    var header: HTTPHeaders? {
        switch self {
        case .photos:
            return nil
        case .user:
            return nil
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
}
