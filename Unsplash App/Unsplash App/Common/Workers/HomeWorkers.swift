//
//  HomeWorkers.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import Foundation

protocol HomeServiceProtocol {
    func getPhotos(request: AllData.Photos.Request, completion: @escaping (Result<[AllData.Photos.Response], HomeError>) -> Void)
    func getUser(request: AllData.User.Request, completion: @escaping (Result<AllData.User.Response, HomeError>) -> Void)
}

class HomeWorker: HomeServiceProtocol{
    var homeService: HomeServiceProtocol
    
    init(homeService: HomeServiceProtocol = HomeAPI()) {
        self.homeService = homeService
    }
    
    func getPhotos(request: AllData.Photos.Request, completion: @escaping (Result<[AllData.Photos.Response], HomeError>) -> Void) {
        homeService.getPhotos(request: request, completion: { result in
            completion(result)
        })
    }
    
    func getUser(request: AllData.User.Request, completion: @escaping (Result<AllData.User.Response, HomeError>) -> Void) {
        homeService.getUser(request: request, completion: { result in
            completion(result)
        })
    }
}
