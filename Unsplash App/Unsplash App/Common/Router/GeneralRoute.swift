//
//  GeneralRoute.swift
//  Marvel Characters
//
//  Created by Carlos Villamizar on 11/11/20.
//

import Foundation
import UIKit

enum GeneralRoute: IRouter {
    case main
    case home
    case detail
    case favorite
}

extension GeneralRoute {
    var scene: UIViewController? {
        switch self {
        case .main:
            return MainViewController()
        case .home:
            return HomeViewController()
        case .detail:
            return PhotoDetailViewController()
        case .favorite:
            return FavoriteViewController()
        }
    }
}
