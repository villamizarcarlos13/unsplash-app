//
//  SettingsRouter.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import Foundation

@objc protocol SettingsRoutingLogic {
}

protocol SettingsDataPassing {
    var dataStore: SettingsDataStore? { get }
}

class SettingsRouter: NSObject, SettingsRoutingLogic, SettingsDataPassing {
    weak var viewController: SettingsViewController?
    var dataStore: SettingsDataStore?
  
}
