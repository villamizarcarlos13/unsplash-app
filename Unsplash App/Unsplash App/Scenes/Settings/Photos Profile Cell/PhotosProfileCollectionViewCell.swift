//
//  PhotosProfileCollectionViewCell.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 4/3/21.
//

import UIKit

class PhotosProfileCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView    : MCImageView!
    
    static let reuseIdentifier      = "photoProfileViewCell"
    static let cellHeight           : CGFloat = 290
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
