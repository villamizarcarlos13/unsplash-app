//
//  SettingsInteractor.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import Foundation

protocol SettingsBusinessLogic {
}

protocol SettingsDataStore {
}

class SettingsInteractor: SettingsBusinessLogic, SettingsDataStore {
    var presenter: SettingsPresentationLogic?
    var worker: HomeServiceProtocol
    
    init(worker: HomeServiceProtocol = HomeWorker()) {
        self.worker = worker
    }
}
