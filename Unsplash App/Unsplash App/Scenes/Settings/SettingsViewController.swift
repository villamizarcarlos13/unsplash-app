//
//  SettingsViewController.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import UIKit

protocol SettingsDisplayLogic: class {
}

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var profileImageView             : MCImageView!
    @IBOutlet weak var nameLabel                    : UILabel!
    @IBOutlet weak var overviewLabel                : UILabel!
    @IBOutlet weak var photosLabel                  : UILabel!
    @IBOutlet weak var collectionsLabel             : UILabel!
    @IBOutlet weak var likesLabel                   : UILabel!
    @IBOutlet weak var locationLabel                : UILabel!
    @IBOutlet weak var photosCollectionView         : UICollectionView!
    @IBOutlet weak var collectionsCollectionView    : UICollectionView!
    
    let cellPhotosNibName = "PhotosProfileCollectionViewCell"
    
    var allPhotos : [PhotosModels]?
    
    var interactor: SettingsBusinessLogic?
    var router: (NSObjectProtocol & SettingsRoutingLogic & SettingsDataPassing)?

    // MARK: - Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
  
    // MARK: - Setup
    private func setup() {
        let viewController = self
        let interactor = SettingsInteractor()
        let presenter = SettingsPresenter()
        let router = SettingsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsCollectionView()
        settingsUI()
    }
    
    //MARK: - Methods
    func settingsCollectionView(){
        photosCollectionView.register(UINib(nibName: cellPhotosNibName, bundle: nil),
                                forCellWithReuseIdentifier: PhotosProfileCollectionViewCell.reuseIdentifier)
    }
    
    func settingsUI(){
        switch UASettings.current.user {
        case .logedIn(let user):
            profileImageView.setImage(urlString: user.profile_image?.medium, imageView: profileImageView)
            nameLabel.text          = user.name
            overviewLabel.text      = user.last_name
            photosLabel.text        = "\(user.total_photos ?? 0)"
            collectionsLabel.text   = "\(user.total_collections ?? 0)"
            likesLabel.text         = "\(user.total_likes ?? 0)"
            locationLabel.text      = user.location
            allPhotos               = user.photos
        case .none:
            break
        }
    }
}

// MARK: - SettingsDisplayLogic
extension SettingsViewController: SettingsDisplayLogic {
    
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
extension SettingsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allPhotos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = photosCollectionView.dequeueReusableCell(withReuseIdentifier: PhotosProfileCollectionViewCell.reuseIdentifier,
                                                            for: indexPath) as? PhotosProfileCollectionViewCell else {
            fatalError()
        }

        cell.imageView.setImage(urlString: allPhotos?[indexPath.row].urls?.full, imageView: cell.imageView)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: PhotosCollectionViewCell.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
    }
}
