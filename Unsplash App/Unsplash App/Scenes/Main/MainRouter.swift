//
//  MainRouter.swift
//  Food Wallet
//
//  Created by Carlos Villamizar on 21/1/21.
//

import Foundation

@objc protocol MainRoutingLogic {
}

protocol MainDataPassing {
    var dataStore: MainDataStore? { get }
}

class MainRouter: NSObject, MainRoutingLogic, MainDataPassing {
    weak var viewController: MainViewController?
    var dataStore: MainDataStore?
  
}

extension MainRouter: BenefitRouterDelegate {
    func displayPeopleScreen() {
        viewController?.didSelect(item: .coupons)
        viewController?.bottomBar.select(item: .coupons)
    }
}
