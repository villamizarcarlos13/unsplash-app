//
//  MainInteractor.swift
//  Food Wallet
//
//  Created by Carlos Villamizar on 21/1/21.
//

import Foundation

protocol MainBusinessLogic {
    func loadInitialData(request: Main.InitialData.Request)
}

protocol MainDataStore {
}

class MainInteractor: MainBusinessLogic, MainDataStore {
    var presenter: MainPresentationLogic?
    var worker: MainWorker?
  
    // MARK: Load Initial Data
    func loadInitialData(request: Main.InitialData.Request) {
        let response = Main.InitialData.Response()
        presenter?.presentInitialData(response: response)
    }
}
