//
//  MainViewController.swift
//  Food Wallet
//
//  Created by Carlos Villamizar on 21/1/21.
//

import UIKit
import SwiftyGif
import AVFoundation
import Lottie

protocol MainDisplayLogic: class {
    func displayInitialData(viewModel: Main.InitialData.ViewModel, on queu: DispatchQueue)
}

class MainViewController: UIViewController {
    
    @IBOutlet weak var contentBottomBarView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var animationView: AnimationView!
    
    let bottomBar = FWBottomBarViewController()
    var itemSelected = ItemBottomBar.benefit
    
    let benefitVC = BenefitViewController()
    let couponsVC = CouponsViewController()
    let deliveryVC = DeliveryViewController()
    let menuVC = MenuViewController()
    var player: AVAudioPlayer?
    var url : URL?
    let logoAnimationView = LogoAnimationView()
    
    var interactor: MainBusinessLogic?
    var router: (NSObjectProtocol & MainRoutingLogic & MainDataPassing)?

    // MARK: - Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
  
    // MARK: - Setup
    private func setup() {
        let viewController = self
        let interactor = MainInteractor()
        let presenter = MainPresenter()
        let router = MainRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.clearBackground()
        
        var path = Bundle.main.path(forResource: "init.mp3", ofType:nil)!
        
        let target = FWSettings.current.targetName
        
        FWSettings.current.colorPrimary = UIColor(named: "\(target)Primary")
        FWSettings.current.colorSecondary = UIColor(named: "\(target)Secondary")
        FWSettings.current.colorDefault = UIColor(named: "\(target)Default")
        FWSettings.current.colorBackground = UIColor(named: "\(target)Background")
        FWSettings.current.logoApp = target
        
        switch target {
        case "Tommy":
            FWSettings.current.appIdentifier = "1"
            FWSettings.current.appKey = "8d5853ed-0cdf-4c33-a2b6-9a014c8c7e20"
            FWSettings.current.urlApp = "https://www.tommybeans.cl"
            FWSettings.current.urlPedidosYa = "https://www.pedidosya.cl/cadenas/tommy-beans"
            FWSettings.current.urlRappi = "https://rappi.app.link/TommyBeans"
            FWSettings.current.urlUber = "https://www.ubereats.com/cl/brand/tommy-beans/?utm_source=tommybeans_cl&utm_medium=brandpage&utm_campaign=instagram_0420"
            FWSettings.current.urlStore = "https://apps.apple.com/cl/app/juan-maestro/id1527190783)"
        case "Juan":
            FWSettings.current.appIdentifier = "3"
            FWSettings.current.appKey = "d0573c3d-7547-4e9f-8c00-894c0dec2fed"
            FWSettings.current.urlApp = "https://www.juanmaestro.cl"
            FWSettings.current.urlPedidosYa = "https://www.pedidosya.cl/cadenas/juan-maestro"
            FWSettings.current.urlRappi = "https://rappi.app.link/IrAJuanMaestro"
            FWSettings.current.urlUber = "https://www.ubereats.com/cl/brand/juan-maestro/?utm_source=juanmaestro_cl&utm_medium=brandpage&utm_campaign=instagram_0420"
            FWSettings.current.urlStore = "https://apps.apple.com/cl/app/juan-maestro/id1527190783)"
        case "Mamut":
            FWSettings.current.appIdentifier = "2"
            FWSettings.current.appKey = "ae52fb54-891a-4ea3-b5ff-28a05db447bb"
            FWSettings.current.urlApp = "https://www.mamutrestaurante.cl"
            FWSettings.current.urlPedidosYa = "https://www.pedidosya.cl/cadenas/mamut"
            FWSettings.current.urlRappi = "https://rappi.app.link/IrAMamut"
            FWSettings.current.urlUber = "https://www.ubereats.com/cl/brand/mamut/?utm_source=mamut_cl&utm_medium=brandpage&utm_campaign=instagram_0420"
            FWSettings.current.urlStore = "https://apps.apple.com/cl/app/juan-maestro/id1527190783)"
        case "Doggis":
            FWSettings.current.appIdentifier = "4"
            FWSettings.current.appKey = "58e353e8-4064-4b88-8f32-69f8a0212d48"
            FWSettings.current.urlApp = "https://www.doggis.cl"
            FWSettings.current.urlPedidosYa = "https://www.pedidosya.cl/cadenas/doggis"
            FWSettings.current.urlRappi = "https://rappi.app.link/IrADoggis"
            FWSettings.current.urlUber = "https://www.ubereats.com/cl/brand/doggis/?utm_source=doggis_cl&utm_medium=brandpage&utm_campaign=instagram_0420"
            FWSettings.current.urlStore = "https://apps.apple.com/cl/app/juan-maestro/id1527190783)"
            path = Bundle.main.path(forResource: "init.mp3", ofType:nil)!
        default:
            break
        }
        
        
        animationView.animation = Animation.named(target)
        
        animationView.contentMode = .scaleAspectFill
          
        // 2. Set animation loop mode

        animationView.loopMode = .playOnce

        // 3. Adjust animation speed

        animationView.animationSpeed = 0.9
        
        url = URL(fileURLWithPath: path)
        
        do {
            player = try AVAudioPlayer(contentsOf: url!)
            player?.play()
            animationView.play { (finished) in
                self.animationView.isHidden = true
                Helper.callWelcomeView(vc: self)
                self.contentView.isHidden = false
                self.contentBottomBarView.isHidden = false
                self.settingsNavBar()
            }
            
        } catch {
            print("error")
        }

        settingBottomBar()
        
        settingFirstViewController()
        loadInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - Methods
    private func settingBottomBar() {
        add(child: bottomBar, container: contentBottomBarView)
        bottomBar.view.frame = contentBottomBarView.bounds
        bottomBar.delegate = self
    }
    
    private func settingFirstViewController() {
        add(child: benefitVC, container: contentView)
        benefitVC.view.frame = contentView.bounds
        (benefitVC.router as! BenefitRouter).delegate = router as? BenefitRouterDelegate
    }
    
    func settingsNavBar() {
        let logoImage    = UIImageView(image: UIImage(named: FWSettings.current.logoApp ?? ""))
        let userImage    = UIImage(named: "bell")!
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        
        let userButton = UIBarButtonItem(image: userImage,  style: .plain, target: self, action: #selector(didTapDeleteButton(sender:)))
        
        navigationController?.setBackground()
        
        if FWSettings.current.appIdentifier == "2" || FWSettings.current.appIdentifier == "3" {
            userButton.tintColor = .white
        }else{
            userButton.tintColor = FWSettings.current.colorSecondary
        }
        
        logoImage.frame = CGRect(x: 0, y: 0, width: 150, height: 40)
        logoImage.contentMode = .scaleAspectFit

        titleView.addSubview(logoImage)
        titleView.backgroundColor = .clear
        
        navigationItem.rightBarButtonItems = [userButton]
        navigationItem.titleView = titleView
    }
    
    @objc func didTapEditButton(sender: AnyObject){
        print("Touch")
    }
    
    @objc func didTapDeleteButton(sender: AnyObject){
        print("Touch")
    }
    
    // MARK: - Load Initial Data
    private func loadInitialData() {
        let request = Main.InitialData.Request()
        interactor?.loadInitialData(request: request)
    }
}

// MARK: - MainDisplayLogic
extension MainViewController: MainDisplayLogic {
    func displayInitialData(viewModel: Main.InitialData.ViewModel, on queu: DispatchQueue = .main) {
        bottomBar.setValues(items: viewModel.items)
    }
}

// MARK: - MainBottomLogic
extension MainViewController: FWBottomBarViewControllerDelegate {
    func didSelect(item: ItemBottomBar) {
        if itemSelected != item {
            removeVC(item: itemSelected)
            itemSelected = item
            addVC(item: item)
        }
    }
    
    private func addVC(item: ItemBottomBar) {
        switch item {
        case .benefit:
            settingFirstViewController()
        case .coupons:
            add(child: couponsVC, container: contentView)
            couponsVC.view.frame = contentView.bounds
        case .delivery:
            add(child: deliveryVC, container: contentView)
            deliveryVC.view.frame = contentView.bounds
        case .menu:
            add(child: menuVC, container: contentView)
            menuVC.view.frame = contentView.bounds
        }
    }
    
    private func removeVC(item: ItemBottomBar) {
        switch item {
        case .benefit:
            benefitVC.remove()
        case .coupons:
            couponsVC.remove()
        case .delivery:
            deliveryVC.remove()
        case .menu:
            benefitVC.remove()
        }
    }
}
