//
//  MainPresenter.swift
//  Food Wallet
//
//  Created by Carlos Villamizar on 21/1/21.
//

import Foundation

protocol MainPresentationLogic {
    func presentInitialData(response: Main.InitialData.Response)
}

class MainPresenter: MainPresentationLogic {
    weak var viewController: MainDisplayLogic?
  
    // MARK: - Present Initial Data
    func presentInitialData(response: Main.InitialData.Response) {
        let viewModel = Main.InitialData.ViewModel(items: response.items)
        viewController?.displayInitialData(viewModel: viewModel, on: .main)
    }
}
