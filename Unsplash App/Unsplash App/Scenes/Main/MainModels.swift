//
//  MainModels.swift
//  Food Wallet
//
//  Created by Carlos Villamizar on 21/1/21.
//

import Foundation

enum Main {
    // MARK: Use cases
    enum InitialData {
        struct Request {
        }
        struct Response {
            let items = ItemBottomBar.allCases
        }
        struct ViewModel {
            var items: [ItemBottomBar]
        }
    }
}
