//
//  MainPresenter.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import UIKit

protocol MainPresentationLogic {
    func presentInitialData(response: Main.InitialData.Response)
    func presentError(response: Main.Error.Response)
}

class MainPresenter: MainPresentationLogic {
    weak var viewController: MainDisplayLogic?
  
    // MARK: - Present Initial Data
    func presentInitialData(response: Main.InitialData.Response) {
        let viewModel = Main.InitialData.ViewModel(items: response.items)
        viewController?.displayInitialData(viewModel: viewModel, on: .main)
    }
    
    func presentError(response: Main.Error.Response) {
        let viewModel = Main.Error.ViewModel(error: response.error)
        viewController?.displayError(viewModel: viewModel, on: .main)
    }
}
