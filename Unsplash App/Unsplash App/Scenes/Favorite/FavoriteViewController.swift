//
//  FavoriteViewController.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import UIKit

protocol FavoriteDisplayLogic: class {
}

class FavoriteViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: MCImageView!
    
    var interactor: FavoriteBusinessLogic?
    var router: (NSObjectProtocol & FavoriteRoutingLogic & FavoriteDataPassing)?

    // MARK: - Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
  
    // MARK: - Setup
    private func setup() {
        let viewController = self
        let interactor = FavoriteInteractor()
        let presenter = FavoritePresenter()
        let router = FavoriteRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        settingsUI()
    }
    
    func settingsUI(){
        switch UASettings.current.user {
        case .logedIn(let user):
            profileImageView.setImage(urlString: user.profile_image?.medium, imageView: profileImageView)
        case .none:
            break
        }
    }
}

// MARK: - FavoriteDisplayLogic
extension FavoriteViewController: FavoriteDisplayLogic {
    
}

