//
//  FavoriteRouter.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import Foundation

@objc protocol FavoriteRoutingLogic {
}

protocol FavoriteDataPassing {
    var dataStore: FavoriteDataStore? { get }
}

class FavoriteRouter: NSObject, FavoriteRoutingLogic, FavoriteDataPassing {
    weak var viewController: FavoriteViewController?
    var dataStore: FavoriteDataStore?
  
}
