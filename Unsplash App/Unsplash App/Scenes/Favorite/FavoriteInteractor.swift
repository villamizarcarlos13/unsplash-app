//
//  FavoriteInteractor.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import Foundation

protocol FavoriteBusinessLogic {
}

protocol FavoriteDataStore {
}

class FavoriteInteractor: FavoriteBusinessLogic, FavoriteDataStore {
    var presenter: FavoritePresentationLogic?
    var worker: HomeServiceProtocol
    
    init(worker: HomeServiceProtocol = HomeWorker()) {
        self.worker = worker
    }
}
