//
//  HomePresenter.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import Foundation

protocol HomePresentationLogic {
    func presentPhotos(response: [AllData.Photos.Response])
    func presentError(response: AllData.Error.Response)
    func presentUser(response: AllData.User.Response)
}

class HomePresenter: HomePresentationLogic {
    weak var viewController: HomeDisplayLogic?
  
    // MARK: - Present Initial Data
    func presentPhotos(response: [AllData.Photos.Response]){
        let viewModel = AllData.Photos.ViewModel(data: response)
        viewController?.displayPhotos(viewModel: viewModel, on: .main)
    }
    
    func presentUser(response: AllData.User.Response) {
        let viewModel = AllData.User.ViewModel(data: response)
        viewController?.displayUser(viewModel: viewModel, on: .main)
    }
    
    func presentError(response: AllData.Error.Response){
        let viewModel = AllData.Error.ViewModel(error: response.error)
        viewController?.displayError(viewModel: viewModel, on: .main)
    }
}
