//
//  PhotosCollectionViewCell.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var headerImageView  : MCImageView!
    @IBOutlet weak var profileImageView : MCImageView!
    @IBOutlet weak var nameLabel        : UILabel!
    @IBOutlet weak var likeLabel        : UILabel!

    static let reuseIdentifier          = "photoViewCell"
    static let cellHeight               : CGFloat = 290
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configUI(data: AllData.Photos.Response) {
        headerImageView.setImage(urlString: data.urls?.full, imageView: headerImageView)
        profileImageView.setImage(urlString: data.user?.profile_image?.medium, imageView: profileImageView)
        nameLabel.text = "\(data.user?.first_name ?? "") \(data.user?.last_name ?? "")"
        likeLabel.text = "Likes: \(data.likes ?? 0)"
    }

}
