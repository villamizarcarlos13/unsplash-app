//
//  HomeInteractor.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import Foundation

protocol HomeBusinessLogic {
    func loadInitialData(request: AllData.Photos.Request)
    func loadUserData(request: AllData.User.Request)
}

protocol HomeDataStore {
}

class HomeInteractor: HomeBusinessLogic, HomeDataStore {
    var presenter: HomePresentationLogic?
    var worker: HomeServiceProtocol
    
    init(worker: HomeServiceProtocol = HomeWorker()) {
        self.worker = worker
    }
  
    // MARK: Get Initial data
    func loadInitialData(request: AllData.Photos.Request) {
        worker.getPhotos(request: request) { result in
            switch result {
            case .success(let response):
                self.presenter?.presentPhotos(response: response)
            case .failure(let error):
                let response = AllData.Error.Response(error: error)
                self.presenter?.presentError(response: response)
            }
        }
    }
    
    // MARK: Get User data
    func loadUserData(request: AllData.User.Request) {
        worker.getUser(request: request) { result in
            switch result {
            case .success(let response):
                self.presenter?.presentUser(response: response)
            case .failure(let error):
                let response = AllData.Error.Response(error: error)
                self.presenter?.presentError(response: response)
            }
        }
    }
}
