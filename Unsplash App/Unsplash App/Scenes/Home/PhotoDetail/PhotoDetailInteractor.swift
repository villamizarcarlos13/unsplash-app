//
//  PhotoDetailInteractor.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import Foundation

protocol PhotoDetailBusinessLogic {
}

protocol PhotoDetailDataStore {
}

class PhotoDetailInteractor: PhotoDetailBusinessLogic, PhotoDetailDataStore {
    var presenter: PhotoDetailPresentationLogic?
    var worker: HomeServiceProtocol
    
    init(worker: HomeServiceProtocol = HomeWorker()) {
        self.worker = worker
    }
}
