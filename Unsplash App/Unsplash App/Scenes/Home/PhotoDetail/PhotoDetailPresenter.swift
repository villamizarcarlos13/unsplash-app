//
//  PhotoDetailPresenter.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import Foundation

protocol PhotoDetailPresentationLogic {
}

class PhotoDetailPresenter: PhotoDetailPresentationLogic {
    weak var viewController: PhotoDetailDisplayLogic?
}
