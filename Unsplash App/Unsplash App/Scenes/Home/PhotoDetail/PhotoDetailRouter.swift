//
//  PhotoDetailRouter.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import Foundation

@objc protocol PhotoDetailRoutingLogic {
}

protocol PhotoDetailDataPassing {
    var dataStore: PhotoDetailDataStore? { get }
}

class PhotoDetailRouter: NSObject, PhotoDetailRoutingLogic, PhotoDetailDataPassing {
    weak var viewController: PhotoDetailViewController?
    var dataStore: PhotoDetailDataStore?
  
}
