//
//  PhotoDetailViewController.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 2/3/21.
//

import UIKit

protocol PhotoDetailDisplayLogic: class {
}


class PhotoDetailViewController: BaseViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var namePhotoLabel: UILabel!
    @IBOutlet weak var namePersonLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var profileButton: FWButton!
    
    
    var detailt : AllData.Photos.Response?
    
    var interactor: PhotoDetailBusinessLogic?
    var router: (NSObjectProtocol & PhotoDetailRoutingLogic & PhotoDetailDataPassing)?

    // MARK: - Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
  
    // MARK: - Setup
    private func setup() {
        let viewController = self
        let interactor = PhotoDetailInteractor()
        let presenter = PhotoDetailPresenter()
        let router = PhotoDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        settingsUI()
        setupBackButton()
    }
    
    //MARK: - Methods
    func settingsUI() {
        imageView.setImage(urlString: detailt?.urls?.full, imageView: imageView) // Capturar foto
        
        profileButton.setTitle("Autor Profile", for: .normal)
        
        namePhotoLabel.text = "Description: \(detailt?.description ?? "")"
        namePersonLabel.text = "Autor: \(detailt?.user?.first_name ?? "") \(detailt?.user?.last_name ?? "")"
        likesLabel.text = "\(detailt?.likes ?? 1)"
    }
    
    //MARK: - Actions
    @IBAction func onClickProfile(_ sender: Any) {
        
    }
}

// MARK: - PhotoDetailDisplayLogic
extension PhotoDetailViewController: PhotoDetailDisplayLogic {
    
}
