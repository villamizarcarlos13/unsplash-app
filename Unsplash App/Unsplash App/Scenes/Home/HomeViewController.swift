//
//  HomeViewController.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import UIKit

protocol HomeDisplayLogic: class {
    func displayPhotos(viewModel: AllData.Photos.ViewModel, on queu: DispatchQueue)
    func displayError(viewModel: AllData.Error.ViewModel, on queu: DispatchQueue)
    func displayUser(viewModel: AllData.User.ViewModel, on queu: DispatchQueue)
}

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var profileImageView : MCImageView!
    @IBOutlet weak var collectionView   : UICollectionView!
    
    let cellPhotosNibName = "PhotosCollectionViewCell"
    
    var allData : [AllData.Photos.Response]?

    var interactor: HomeBusinessLogic?
    var router: (NSObjectProtocol & HomeRoutingLogic & HomeDataPassing)?

    // MARK: - Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
  
    // MARK: - Setup
    private func setup() {
        let viewController = self
        let interactor = HomeInteractor()
        let presenter = HomePresenter()
        let router = HomeRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsCollectionView()
        getUserData()
    }
    
    //MARK: - Methods
    func settingsCollectionView(){
        collectionView.register(UINib(nibName: cellPhotosNibName, bundle: nil),
                                forCellWithReuseIdentifier: PhotosCollectionViewCell.reuseIdentifier)
    }
    
    //MARK: - Send Request
    func getAllData() {
        let request = AllData.Photos.Request()
        interactor?.loadInitialData(request: request)
    }
    
    //MARK: - Send Request User
    func getUserData(){
        let request = AllData.User.Request()
        interactor?.loadUserData(request: request)
    }
}

// MARK: - HomeDisplayLogic
extension HomeViewController: HomeDisplayLogic {
    func displayPhotos(viewModel: AllData.Photos.ViewModel, on queu: DispatchQueue = .main){
        allData = viewModel.data
        
        collectionView.reloadData()
        
    }
    
    func displayUser(viewModel: AllData.User.ViewModel, on queu: DispatchQueue = .main) {
        getAllData()
        
        UASettings.current.user = .logedIn(viewModel.data)
        
        profileImageView.setImage(urlString: viewModel.data.profile_image?.medium, imageView: profileImageView)
    }
    
    func displayError(viewModel: AllData.Error.ViewModel, on queu: DispatchQueue = .main){
        displaySimpleAlert(with: "Error", message: viewModel.error.description)
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotosCollectionViewCell.reuseIdentifier,
                                                            for: indexPath) as? PhotosCollectionViewCell else {
            fatalError()
        }

        cell.configUI(data: (allData?[indexPath.row])!)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 20, height: PhotosCollectionViewCell.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewCtrl = PhotoDetailViewController()
        viewCtrl.detailt = allData?[indexPath.row]
        
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
}
