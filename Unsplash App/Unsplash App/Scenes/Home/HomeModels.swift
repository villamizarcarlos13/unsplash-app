//
//  HomeModels.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import Foundation

enum AllData {
    // MARK: Use cases
    enum Photos {
        struct Request: Codable {
        }

        struct Response: Codable {
            // MARK: - DataClass
            struct DataClass: Codable {
                
            }
            let id              : String
            let urls            : ImagesData?
            let likes           : Int?
            let user            : UserModels?
            let description     : String?
            let collection      : [Collection]?

            enum CodingKeys: String, CodingKey {
                case id
                case urls
                case likes
                case user
                case description
                case collection
            }
        }

        struct ViewModel {
            let data: [AllData.Photos.Response]
        }
    }
    
    enum User{
        struct Request: Codable {
        }

        struct Response: Codable {
            // MARK: - DataClass
            struct DataClass: Codable {
                
            }
            let id                  : String?
            let username            : String?
            let name                : String?
            let first_name          : String?
            let last_name           : String?
            let profile_image       : ImageProfile?
            let location            : String?
            let total_collections   : Int?
            let total_likes         : Int?
            let total_photos        : Int?
            let photos              : [PhotosModels]?

            enum CodingKeys: String, CodingKey {
                case id
                case username
                case name
                case first_name
                case last_name
                case profile_image
                case location
                case total_collections
                case total_likes
                case total_photos
                case photos
            }
        }

        struct ViewModel {
            let data: AllData.User.Response
        }
    }
    
    enum Error {
        struct Request {
        }
        struct Response {
            var error: HomeError
        }
        struct ViewModel {
            var error: HomeError
        }
    }
}
