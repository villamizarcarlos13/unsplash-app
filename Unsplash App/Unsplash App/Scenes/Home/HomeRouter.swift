//
//  HomeRouter.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import Foundation

@objc protocol HomeRoutingLogic {
    func routeToDetail()
}

protocol HomeDataPassing {
    var dataStore: HomeDataStore? { get }
}

class HomeRouter: NSObject, HomeRoutingLogic, HomeDataPassing {
    weak var viewController: HomeViewController?
    var dataStore: HomeDataStore?
  
    //MARK: - Routing
    func routeToDetail(){
        navigateToDetail(source: viewController!, destination: GeneralRoute.detail)
    }
    
    // MARK: - Navigation
    func navigateToDetail(source: HomeViewController, destination: GeneralRoute) {
        guard let benefitDetailVC = destination.scene else { return }
        
        source.parent?.navigationController?.pushViewController(benefitDetailVC, animated: true)
    }
}
