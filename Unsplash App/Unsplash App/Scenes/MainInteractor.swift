//
//  MainInteractor.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import UIKit
import Foundation

protocol MainBusinessLogic {
    func loadInitialData(request: Main.InitialData.Request)
}

protocol MainDataStore {
}

class MainInteractor: MainBusinessLogic, MainDataStore {
   
    var presenter: MainPresentationLogic?

    // MARK: Load Initial Data
    func loadInitialData(request: Main.InitialData.Request) {
        let response = Main.InitialData.Response()
        presenter?.presentInitialData(response: response)
    }
}
