//
//  MainViewController.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import UIKit
import Kingfisher

protocol MainDisplayLogic: class {
    func displayInitialData(viewModel: Main.InitialData.ViewModel, on queu: DispatchQueue)
    func displayError(viewModel: Main.Error.ViewModel, on queu: DispatchQueue)
}

class MainViewController: BaseViewController {
    
    @IBOutlet weak var contentBottomBarView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    var notificationImage = UIImageView()

    let bottomBar       = FWBottomBarViewController()
    let homeVC          = HomeViewController()
    let favoriteVC        = FavoriteViewController()
    let profileVC       = SettingsViewController()
    var itemSelected    = ItemBottomBar.home
    
    var interactor: MainBusinessLogic?
    var router: (NSObjectProtocol & MainRoutingLogic & MainDataPassing)?
    
    // MARK: - Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        let viewController = self
        let interactor = MainInteractor()
        let presenter = MainPresenter()
        let router = MainRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.clearBackground()
        
        settingBottomBar()
        settingFirstViewController()
        loadInitialData()
    }
    
    // MARK: - Methods
    private func settingBottomBar() {
        add(child: bottomBar, container: contentBottomBarView)
        bottomBar.view.frame = contentBottomBarView.bounds
        bottomBar.delegate = self
    }
    
    private func settingFirstViewController() {
        add(child: homeVC, container: contentView)
        homeVC.view.frame = contentView.bounds
    }
    
    func navBarTitle(title: String, name: String){
        let label = UILabel()
        label.attributedText = attributedText(withString: String(format: "\(title) %@", name), boldString: name, font: UIFont(name: "Rubik", size: 18)!)
        label.frame = CGRect(x: 0.0, y: 10.0, width: 200, height: 25)
        label.textColor = .blue
        
        let customView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 44.0))
        customView.addSubview(label)
        let leftButton = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    func navBarLogo(state: Bool, profilePhoto: String) {
        //add Button en el lado derecho
        let customView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 44.0))
        let button = UIButton.init(type: .custom)
        button.setBackgroundImage(UIImage(named: "default-photo"), for: .normal)
        let profileImage = UIImageView()
        notificationImage = UIImageView(frame: CGRect(x: 61, y: 13, width: 18, height: 18))
        
        if state {
            profileImage.kf.setImage(with: URL(string: profilePhoto ),
                                     placeholder: UIImage(named: "default-photo"),
                                     options: [.cacheMemoryOnly,
                                               .waitForCache,
                                               .transition(.fade(1))],
                                     completionHandler: { [weak self] result in
                                        switch result {
                                        case let .success(value):
                                            button.setBackgroundImage(value.image, for: .normal)
                                        case let .failure(error):
                                            print(error.localizedDescription)
                                            button.setBackgroundImage(UIImage(named: "default-photo"), for: .normal)
                                            self?.notificationImage.image = UIImage(named: "Notification")
                                        }
                                     })
        }
        
        button.frame = CGRect(x: 70.0, y: 5.0, width: 35, height: 35)
        button.layer.cornerRadius = button.bounds.height / 2
        button.clipsToBounds = true
        customView.addSubview(button)
        customView.addSubview(notificationImage)
        let rightButton = UIBarButtonItem(customView: customView)
        self.navigationItem.rightBarButtonItem = rightButton
        
//        button.addTarget(self, action: #selector(notificationMenu(sender:)), for: .touchUpInside)
    }
    
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSAttributedString.Key.font: font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont(name: "Rubik-Medium", size: 18.0)!]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
    
    // MARK: - Load Initial Data
    private func loadInitialData() {
        let request = Main.InitialData.Request()
        interactor?.loadInitialData(request: request)
    }
}

// MARK: - MainDisplayLogic
extension MainViewController: MainDisplayLogic {
    
    func displayInitialData(viewModel: Main.InitialData.ViewModel, on queu: DispatchQueue = .main) {
        bottomBar.setValues(items: viewModel.items)
    }
    
    func displayError(viewModel: Main.Error.ViewModel, on queu: DispatchQueue = .main) {
        displaySimpleAlert(with: "Error", message: viewModel.error.description)
    }
}

// MARK: - MainDisplayLogic
extension MainViewController: FWBottomBarViewControllerDelegate {
    func didSelect(item: ItemBottomBar) {
        if itemSelected != item {
            removeVC(item: itemSelected)
            itemSelected = item
            addVC(item: item)
        }
    }
    
    private func addVC(item: ItemBottomBar) {
        switch item {
        case .home:
            settingFirstViewController()
        case .favorites:
            add(child: favoriteVC, container: contentView)
            favoriteVC.view.frame = contentView.bounds
        case .profile:
            add(child: profileVC, container: contentView)
            profileVC.view.frame = contentView.bounds
        }
    }
    
    private func removeVC(item: ItemBottomBar) {
        switch item {
        case .home:
            homeVC.remove()
        case .favorites:
            favoriteVC.remove()
        case .profile:
            profileVC.remove()
        }
    }
}
