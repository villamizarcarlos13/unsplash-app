//
//  MainModels.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import UIKit

enum Main {
    // MARK: Use cases
    enum InitialData {
        struct Request {
        }
        struct Response {
            let items = ItemBottomBar.allCases
        }
        struct ViewModel {
            var items: [ItemBottomBar]
        }
    }
    
    enum Error {
        struct Request {
        }
        struct Response {
            let error: MainError
        }
        struct ViewModel {
            let error: MainError
        }
    }
}
