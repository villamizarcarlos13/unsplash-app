//
//  AppDelegate.swift
//  Unsplash App
//
//  Created by Carlos Villamizar on 1/3/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window : UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow()
        
        window?.setRootViewController(UINavigationController(rootViewController: GeneralRoute.main.scene!), options: .init(direction: .toTop, style: .easeInOut))
        
        return true
    }
}

